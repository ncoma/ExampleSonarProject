﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace ClassLibrary3
{
    public class ClassTest
    {
        [Fact]
        public void TestMe()
        {
            Class1 class1 = new Class1();
            Assert.Equal(9, class1.Add(7, 2));
        }
    }
}
