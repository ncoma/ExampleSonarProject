﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary3.pojos;
using Xunit;

namespace ClassLibrary3.Test
{
    public class ShootBugTest
    {
        [Theory]
        [InlineData(true, false)]
        [InlineData(false, true)]
        public void TestBugDodges(bool didDodge, bool shouldBeDead)
        {

            Bug bug = new Bug();
            Raygun gun = new Raygun();

            if (didDodge)
            {
                bug.Dodge();
            }

            gun.FireAt(bug);

            if (shouldBeDead)
            {
                Assert.True(bug.IsDead());
            }
            else
            {
                Assert.False(bug.IsDead());
            }
        }
    }
}
