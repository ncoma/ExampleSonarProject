﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary3.pojos;
using Moq;
using Xunit;

namespace ClassLibrary3.Test
{
    public class EmployeeTest
    {
        [Fact]
        public void EmployeDateOfJoiningTest()
        {
            var employee = new Mock<Employee>();
            employee.Setup(x => x.GetDateofJoining(It.IsAny<int>())).Returns(DateTime.Now);
            Console.WriteLine("The date is "+employee.Object.GetDateofJoining(1));
        }

    }
}
