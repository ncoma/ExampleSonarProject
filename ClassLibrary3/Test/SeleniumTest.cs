﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using Xunit;
using ClassLibrary3.PageObjects;

namespace ClassLibrary3.Test
{
    public class SeleniumTest
    {
        IWebDriver Driver = new ChromeDriver();

        [Fact]
        public void MyTest()
        {
            GoogleHome GoogleHome = new GoogleHome(Driver);
            GoogleHome.GoToSite();
            GoogleHome.TypeOnSearchBar("lotr");
            GoogleHome.ClickOnGoogleSearch();
            Assert.True(GoogleHome.VerifyResultStatsExists().Displayed);
        }
    }
}
