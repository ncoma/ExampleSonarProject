﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;

namespace ClassLibrary3.PageObjects
{
    class GoogleHome
    {
        private const String GoogleURL = "http://www.google.com";
        private IWebDriver Driver;
        private IWebElement SearchBar;

        public GoogleHome(IWebDriver Driver)
        {
            this.Driver = Driver;
        }

        public void GoToSite() => Driver.Url = GoogleURL;

        public void TypeOnSearchBar(String keys)
        {
            SearchBar = Driver.FindElement(By.Id("lst-ib"));
            SearchBar.SendKeys(keys);
        }
        public void ClickOnGoogleSearch()
        {
            Driver.FindElement(By.Name("btnK")).Click();
        }
        public IWebElement VerifyResultStatsExists()
        {
            return Driver.FindElement(By.Id("resultStats"));
        }
    }
}
