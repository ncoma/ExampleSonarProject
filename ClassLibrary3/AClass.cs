﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary3.pojos;

namespace ClassLibrary3
{
    class AClass
    {
        public Employee employee { get; }

        public AClass(Employee employee)
        {
            this.employee = employee;
        }

        public String employeNameUpper(int id)
        {
            return employee.GetName(id).ToUpper();
        }
    }
}
